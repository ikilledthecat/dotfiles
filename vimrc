if $UseEnv == 2
  python 1
else
  python3 1
  let g:pymode_python = 'python3'
endif
""""""""""""""""""""""""""""""""""""""""""""VAM Setup"""""""""""""""""""""""""""""""""""""""""""
"
" put this line first in ~/.vimrc
set nocompatible
filetype indent plugin on
syn on
""""""""""""""""""""""""""""""""""""""VAM setup"""""""""""""""""""""""""""""""{{{
fun! EnsureVamIsOnDisk(plugin_root_dir)

  " windows users may want to use http://mawercer.de/~marc/vam/index.php
  " to fetch VAM, VAM-known-repositories and the listed plugins
  " without having to install curl, 7-zip and git tools first
  " -> BUG [4] (git-less installation)

  let vam_autoload_dir = a:plugin_root_dir.'/vim-addon-manager/autoload'

  if isdirectory(vam_autoload_dir)
    return 1
  else
    if 1 == confirm("Clone VAM into ".a:plugin_root_dir."?","&Y\n&N")
      " I'm sorry having to add this reminder. Eventually it'll pay off ".
      \" call confirm("Remind yourself that most plugins ship with ".
      \"documentation (README*, doc/*.txt). It is your ".
      \"first source of knowledge. If you can't find ".
      \"the info you're looking for in reasonable ".
      \"time ask maintainers to improve documentation")
      call mkdir(a:plugin_root_dir, 'p')
      execute '!git clone --depth=1 git://github.com/MarcWeber/vim-addon-manager '.
      \ shellescape(a:plugin_root_dir, 1).'/vim-addon-manager'
      " VAM runs helptags automatically when you install or update
      " plugins
      exec 'helptags '.fnameescape(a:plugin_root_dir.'/vim-addon-manager/doc')
    endif
    return isdirectory(vam_autoload_dir)
  endif
endfun

fun! SetupVAM()
  " Set advanced options like this:
  " let g:vim_addon_manager = {}
  " let g:vim_addon_manager.key = value
  " Pipe all output into a buffer which gets written to disk
  " let g:vim_addon_manager.log_to_buf =1
  " Example: drop git sources unless git is in PATH. Same plugins can
  " be installed from www.vim.org. Lookup MergeSources to get more control
  " let g:vim_addon_manager.drop_git_sources = !executable('git')
  " let g:vim_addon_manager.debug_activation = 1
  " VAM install location:
  let c = get(g:, 'vim_addon_manager', {})
  let g:vim_addon_manager = c
  let c.plugin_root_dir = expand('$HOME/.vim/vim-addons', 1)

  if !EnsureVamIsOnDisk(c.plugin_root_dir)
    echohl ErrorMsg | echomsg "No VAM found!" | echohl NONE
    return
  endif

  let &rtp.=(empty(&rtp)?'':',').c.plugin_root_dir.'/vim-addon-manager'

  " Tell VAM which plugins to fetch & load:
  call vam#ActivateAddons(['neocomplete' ,'unimpaired', 'delimitMate', 'UltiSnips','Syntastic','fugitive', 'taglist'], {'auto_install' : 0})
  " sample: call vam#ActivateAddons(['pluginA','pluginB', ...], {'auto_install' : 0})
  " Also See "plugins-per-line" below
  " Addons are put into plugin_root_dir/plugin-name directory
  " unless those directories exist. Then they are activated.
  " Activating means adding addon dirs to rtp and do some additional
  " magic
  " How to find addon names?
  " - look up source from pool
  " - (<c-x><c-p> complete plugin names):
  " You can use name rewritings to point to sources:
  call vam#ActivateAddons(["Emmet"])
  call vam#ActivateAddons(["github:Shougo/unite.vim"])
  call vam#ActivateAddons(["github:jmcantrell/vim-virtualenv"])
  call vam#ActivateAddons(["github:Shougo/vimproc.vim"])
  call vam#ActivateAddons(["github:Shougo/vimshell.vim"])
  call vam#ActivateAddons(["github:Shougo/neomru.vim"])
  call vam#ActivateAddons(["github:Shougo/vimfiler.vim"])
  call vam#ActivateAddons(["github:tsukkee/unite-tag"])
  call vam#ActivateAddons(["github:bling/vim-airline"])
  call vam#ActivateAddons(["github:tpope/vim-endwise"])
  call vam#ActivateAddons(["github:tpope/vim-surround"])
  call vam#ActivateAddons(["github:tpope/vim-rsi"])
  call vam#ActivateAddons(["github:tpope/vim-rails"])
  call vam#ActivateAddons(["github:tpope/vim-repeat"])
  call vam#ActivateAddons(["github:scrooloose/nerdcommenter"])
  call vam#ActivateAddons(["github:honza/vim-snippets"])
  call vam#ActivateAddons(["github:flazz/vim-colorschemes"])
  "call vam#ActivateAddons([""])
  call vam#ActivateAddons(["github:inside/vim-search-pulse"])
  call vam#ActivateAddons(["github:godlygeek/tabular"])
  call vam#ActivateAddons(["github:tommcdo/vim-exchange"])
  call vam#ActivateAddons(["github:sjl/gundo.vim"])
  call vam#ActivateAddons(["github:Yggdroot/indentLine"])
  call vam#ActivateAddons(["github:justinmk/vim-sneak"])
  "call vam#ActivateAddons(["github:davidhalter/jedi-vim"])
  call vam#ActivateAddons(["github:klen/python-mode"])
  call vam#ActivateAddons(["github:vimwiki/vimwiki"])
  call vam#ActivateAddons(["github:mhinz/vim-startify"])
  call vam#ActivateAddons(["github:mhinz/vim-sayonara"])
  call vam#ActivateAddons(["github:chrisbra/csv.vim"])
  call vam#ActivateAddons(["github:pangloss/vim-javascript"])
  let scripts = []
  call add(scripts, {'name': 'github:klen/python-mode', 'ft_regex':'^\.py$'})
  call vam#Scripts(scripts, {})
  " ..ActivateAddons(["github:user/repo", .. => github://user/repo
  " Also see section "2.2. names of addons and addon sources" in VAM's documentation
endfun

call SetupVAM()
"VAMActivate matchit.zip vim-addon-commenting
  " experimental [E1]: load plugins lazily depending on filetype, See
  " NOTES
  " experimental [E2]: run after gui has been started (gvim) [3]
  " option1: au VimEnter * call SetupVAM()
  " option2: au GUIEnter * call SetupVAM()
  " See BUGS sections below [*]
  " Vim 7.0 users see BUGS section [3]
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}


""""""""""""""""""""""""""""""""""""""" General Setup""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
" Forget being compatible with good ol' vi
" "
set nocompatible

set t_Co=256

"setting 256 color to enable vim backgrounds
colorscheme summerfruit256

" change leader key
let mapleader = " "

" Remember commands
set history=100

" " Get that filetype stuff happening
filetype on
filetype plugin on
filetype indent on

" Turn on that syntax highlighting
syntax on

" " Tabstops are 2 spaces by default
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set autoindent
set backspace=indent,eol,start
set smarttab
set showmatch
set showcmd
set ruler
set scrolloff=5
set sidescrolloff=5
set smartindent
set number
set ignorecase smartcase incsearch wrapscan     " Search method.
set splitright splitbelow                       " splitting direction
set showmatch matchtime=1 matchpairs+=<:>       " parenthesis matching

"folding
set foldenable foldmethod=marker foldlevelstart=99 foldcolumn=3
let g:xml_syntax_folding=1

" Enable omni completion.
set omnifunc=syntaxcomplete#Complete

autocmd FileType python setlocal omnifunc=pymode#rope#completions foldmethod=indent tabstop=4 shiftwidth=4 softtabstop=4
au BufNewFile,BufRead *.yaml,*.yml setf yaml  "Set ftype for yaml files

" Set Paste Toggle
set pastetoggle=<F4>

" "autocomplete feature
set complete-=i

" Why is this not a default
" set nohidden

"confirm on exit
set confirm

" " Don't update the display while executing macros
set lazyredraw

set shell=bash

" " At least let yourself know what mode you're in
set showmode

" " Enable enhanced command-line completion. Presumes you have compiled
" " with +wildmenu. See :help 'wildmenu'
set wildmenu
set wildmode=list:full
set wildignorecase
" Disable output and VCS files
set wildignore=*.o,*.pyc,*.pyo,*.out,*.obj,.git,*.rbc,*.rbo,*.class,.svn,*.gem
" Disable archive files
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz
" Disable temp and backup files
set wildignore+=*.swp,*~,._*

" " Highlight search
set nohlsearch

" " turn off highlighting
nmap <silent> <leader>nh :nohls<CR>

" Automatically read a file that has changed on disk
set autoread

"to ensure airline-vim shows up for single pane windows also
set laststatus=2

" :cd. change working directory to that of the current file
cmap cd. lcd %:p:h

" Cursor Shape block in Normal Mode and Vertical Line in Insert Mode
if &term =~ '^xterm'
  " solid underscore
  let &t_SI .= "\<Esc>[1 q"
  " solid block
  let &t_EI .= "\<Esc>[2 q"
  " 1 or 0 -> blinking block
  " 2 -> solid block
  " 3 -> blinking underscore
  " 4 -> solid underscore
  " Recent versions of xterm (282 or above) also support
  " 5 -> blinking vertical bar
  " 6 -> solid vertical bar
endif

nnoremap ; :
nnoremap : ;

"remove toolbar from gui
set guioptions-=T
set guioptions-=m

" easy split navigation
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>
nnoremap <C-h> <C-w><C-h>

set splitbelow
set splitright


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}


""""""""""""""""""""""""""""""""""""o"General Keybindings"""""""""""""""""""""""""""""""""""""""""""""""""""{{{
" Set Paste Toggle
set pastetoggle=<F4>

"set plus register to X11 clipboard
set clipboard=unnamed

"Set ctrl u to convert text to UPPERCASE
inoremap <c-u> <esc>vbUwi

""Set ctrl d to delete line in insert mode
inoremap <c-d> <esc>ddi

"Set ctrl w to delete word in insert mode
inoremap <c-w> <esc>ldwi

""Exit insert mode comfortably
inoremap jk <esc>

"add space before or after a character in ormal mode
nnoremap <Space><Right> a<Space><Esc>
nnoremap <Space><Left> i<Space><Esc>

" Let's make it easy to edit this file (mnemonic for the key sequence is

" " 'e'dit 'v'imrc)

nnoremap <silent> ,ev :e $MYVIMRC<cr>

" " And to source this file as well (mnemonic for the key sequence is
" " 's'ource 'v'imrc)
nnoremap <silent> ,sv :so $MYVIMRC<cr>


" set text wrapping toggles
nnoremap <silent> ,ww :set invwrap<CR>:set wrap?<CR>

"set j and k to move to next line visually
map j gj
map k gk


" indent
nnoremap > >>
nnoremap < <<
xnoremap > >gv
xnoremap < <gv

" easy saving
nnoremap <Leader>w :w<CR>

"Lets you type :R <command> to run an external command and give you the
"results in a small new buffer. Useful for filtering your file and getting the
"results separately (similar to textmate's filter and open results in new
"document). You can use # (alternate file) to represent your current file
"
" Examples :
"
":R grep foo # :R wc #
command! -complete=shellcmd -nargs=* R belowright 15new | r ! <args>


"delete trailing white spaces on save
autocmd BufWritePre * :%s/\s\+$//e
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Turn persistent undo on
" means that you can undo even when you close a buffer/VIM
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set undodir=~/.vim/temp/undodir
set undofile
nnoremap <expr> <key> &filetype == "qf" ? "<C-w>p" : "<C-w>b"

" remove toolbar from gui
noremap <C-F2> :if &guioptions =~# 'T'<Bar>
      \set guioptions-=T<Bar>
      \set guioptions-=m<Bar>
      \else <Bar>
      \set guioptions+=T<Bar>
      \set guioptions+=m<Bar>
      \endif<CR>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""Syntastic""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
if $VIRTUAL_ENV
  let g:syntastic_python_python_exec = $VIRTUAL_ENV.'/bin/python'
endif
" Syntax check
"
nnoremap <silent> <F8> :SyntasticCheck<CR>
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""""""NerdTree""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
""mapping keys for nerdtree toggle
"nnoremap <silent> ,nn :NERDTreeToggle<cr>
""ignore filetypes of nerdtree
"let NERDTreeIgnore = ['\.py[co]$', '\.o$', '\.swp$', '\.swo$']
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""""""Vimfiler""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
let g:vimfiler_as_default_explorer = 1
nnoremap <C-v>f :VimFiler -no-safe -parent -status -explorer -split<CR>
inoremap <C-v>f <Esc>:VimFiler -no-safe -parent -status -explorer -split<CR>
" Like Textmate icons.
let g:vimfiler_tree_leaf_icon = ' '
let g:vimfiler_tree_opened_icon = '▾'
let g:vimfiler_tree_closed_icon = '▸'
let g:vimfiler_file_icon = '-'
let g:vimfiler_marked_file_icon = '*'
let g:vimfiler_tree_indentation = 4
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

""""""""""""""""""""""""""""""""""""Unite"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
" Start Insert
let g:unite_enable_start_insert = 1
let g:unite_enable_short_source_names = 1
" Enable history yank source
let g:unite_source_history_yank_enable = 1
" Open in bottom right
let g:unite_split_rule = "botright"
" Shorten the default update date of 500ms
let g:unite_update_time = 300
" set up mru limit
let g:unite_source_file_mru_limit = 100
" highlight like my vim
let g:unite_cursor_line_highlight = 'CursorLine'
" format mru
let g:unite_source_file_mru_filename_format = ':~:.'
let g:unite_source_file_mru_time_format = ''
" set up coolguy arrow prompt
let g:unite_prompt = '➜ '
" configuring ag instead of grep
let g:unite_source_grep_command = 'ag'
let g:unite_source_grep_default_opts =
      \ '--line-numbers --nocolor --nogroup -S --hidden --ignore-case --ignore ' .
      \  '''.hg'' --ignore ''.svn'' --ignore ''.git'' --ignore ''.bzr'''
let g:unite_source_grep_recursive_opt = ''
" Save session automatically.
let g:unite_source_session_enable_auto_save = 1
" Use the fuzzy matcher for everything
call unite#filters#matcher_default#use(['matcher_fuzzy'])
" Use the rank sorter for everything
call unite#filters#sorter_default#use(['sorter_rank'])

nnoremap <space>b :Unite -quick-match buffer<cr>
nnoremap <space>/ :Unite grep:.<cr>
nnoremap <leader>ut :<C-u>Unite -split -buffer-name=files -start-insert file_rec/async:!<cr>
nnoremap <leader>uf :<C-u>Unite -split -buffer-name=files -start-insert file<cr>
nnoremap <leader>ur :<C-u>Unite -split -buffer-name=mru -start-insert file_mru<cr>
nnoremap <leader>uy :<C-u>Unite -split -buffer-name=yank history/yank<cr>
nnoremap <leader>ub :<C-u>Unite -split -buffer-name=buffer buffer<cr>
nnoremap <leader>uc :!retag<cr>:Unite -split -start-insert tag<cr>
nnoremap <leader>uj :Unite -split -buffer-name=jumps -start-insert jump<cr>
" Custom mappings for the unite buffer
autocmd FileType unite call s:unite_settings()
function! s:unite_settings()
  " Enable navigation with control-j and control-k in insert mode
  imap <buffer><C-j> <Plug>(unite_select_next_line)
  imap <buffer><C-k> <Plug>(unite_select_previous_line)
  inoremap <silent><buffer><expr> <C-s> unite#do_action('split')
  inoremap <silent><buffer><expr> <C-v> unite#do_action('vsplit')
  inoremap <silent><buffer><expr> <C-t> unite#do_action('tabopen')
  imap <buffer><C-l> <Plug>(unite_redraw)

  nmap <buffer> <ESC> <Plug>(unite_exit)
endfunction
" Set up some custom ignores
call unite#custom_source('file_rec,file_rec/async,file_mru,file,buffer,grep',
\ 'ignore_pattern', join([
\ '\.git/',
\ 'git5/.*/review/',
\ 'google/obj/',
\ 'tmp/',
\ 'lib/Cake/',
\ 'node_modules/',
\ 'vendor/',
\ 'Vendor/',
\ 'app_old/',
\ 'acf-laravel/',
\ 'plugins/',
\ 'bower_components/',
\ '.sass-cache',
\ 'web/wp',
\ '.pyc',
\ '.pyo',
\ '.swp',
\ '.swo',
\ ], '\|'))

"setup ctags with unite

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""""Tags & Taglist"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
"Setting for TagList
nnoremap <leader>tt :TlistToggle<cr>
" Ctags
nnoremap <leader>tl :tnext<cr>
nnoremap <leader>th :tprev<cr>
nnoremap <leader>ta :tselect<cr>
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""Neocomplete""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0

let g:neocomplete#enable_at_startup = 1

let g:neocomplete#auto_completion_start_length = 2

" When a capital letter is included in input, neocomplete does
"	not ignore the upper- and lowercase.
let g:neocomplete#enable_smart_case = 1

" disable autocomplete
"g:neocomplete#disable_auto_complete

" Define dictionary.
"It is a dictionary to connect a dictionary file with file
"type.  The dictionary's key is filetype and comma-separated
"multiple value is a path to a dictionary file.  If the
"variable is unset or has an empty key, the native 'dictionary'
"option will be inherited.  When you set 'text'
"key, you will appoint dictionary files in text mode.  If the
"key is '_', it is loaded in every filetype.
let g:neocomplete#sources#dictionary#dictionaries = {
      \'default' : '',
      \'vimshell' : $HOME.'/.vimshell_hist',
      \'scheme' : $HOME.'/.gosh_completions'
      \}

"This dictionary records keyword patterns to completion.
"This is appointed in regular expression every file type.
"If the key is '_' or 'default', it is used for default
"pattern.
" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
  let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

let g:neocomplete#enable_auto_delimiter = 1

" Plugin key-mappings.
inoremap <expr><C-u>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return neocomplete#close_popup() . "\<CR>"
    " For no inserting <CR> key.
    " return pumvisible() ? neocomplete#close_popup() : '\<CR>'
endfunction

" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" for jedi vim
if !exists('g:neocomplete#force_omni_input_patterns')
  let g:neocomplete#force_omni_input_patterns = {}
endif
let g:neocomplete#force_omni_input_patterns.python =
      \ '\%([^. \t]\.\|^\s*@\|^\s*from\s.\+import \|^\s*from \|^\s*import \|^\s*self\.?\)\w*'
" alternative pattern: '\h\w*\|[^. \t]\.\w*'
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""UltiSnips""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe
" To use python version 2.x: >
"let g:UltiSnipsUsePythonVersion = 2
let g:UltiSnipsExpandTrigger="<C-l>"
let g:UltiSnipsJumpForwardTrigger="<C-g>"
let g:UltiSnipsJumpBackwardTrigger="<C-z>"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}
"""""""""""""""""""""""""""""""""CursorLine""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
set cursorline
let g:vim_search_pulse_mode = 'pattern'
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}
"""""""""""""""""""""""""""""""""Ipython""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
"Ipyton is not installed through vam rather as ftplugin in the relevant folder
"
"Disable vim ipython autocomplete coz i have neocomplet + jedi
let g:ipy_completefunc = ''
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}
"""""""""""""""""""""""""""""""""Airline"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
let g:airline#extensions#tabline#enabled = 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""Python Mode"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
""Turn on plugin

let g:pymode = 1

"Enable automatic virtualenv detection                     *'g:pymode_virtualenv'*
let g:pymode_virtualenv = 1
"Trim unused whitespace on save
let g:pymode_trim_whitespaces = 1
" Override go-to.definition key shortcut to Ctrl-]
let g:pymode_rope_goto_definition_bind = ",d"
"Bind keys
let g:pymode_breakpoint_bind = ',b'
"Turn off autocomplete
let g:pymode_rope_completion = 0
" " Override run current python file key shortcut to Ctrl-Shift-e
let g:pymode_run_bind = ",e"
"
" " Override view python doc key shortcut to Ctrl-Shift-d
let g:pymode_doc_bind = "K"
"Keymap for rename method/function/class/variables under cursor
let g:pymode_rope_rename_bind = ',re'
let g:pymode_rope_rename_module_bind = ',rem'
"Turn on the run code script                                     *'g:pymode_run'*
let g:pymode_run_bind = ',r'
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""Jedi Vim""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
"let g:jedi#goto_assignments_command = ",g"
"let g:jedi#goto_definitions_command = ",d"
"let g:jedi#documentation_command = "K"
"let g:jedi#usages_command = ",n"
"let g:jedi#completions_command = "<C-Space>"
"let g:jedi#rename_command = ",r"
"let g:jedi#show_call_signatures = "1"
"let g:jedi#completions_enabled = 0
"let g:jedi#auto_vim_configuration = 0
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""Vim Sneak""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
 nmap f <Plug>Sneak_f
 nmap F <Plug>Sneak_F
 xmap f <Plug>Sneak_f
 xmap F <Plug>Sneak_F
 omap f <Plug>Sneak_f
 omap F <Plug>Sneak_F
 nmap t <Plug>Sneak_t
 nmap T <Plug>Sneak_T
 xmap t <Plug>Sneak_t
 xmap T <Plug>Sneak_T
 omap t <Plug>Sneak_t
 omap T <Plug>Sneak_T
 let g:sneak#streak = 1
 let g:sneak#s_next = 1
 let g:sneak#use_ic_next = 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""VimWiki""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
let g:vimwiki_list = [{'path': '$HOME/Dropbox/wiki'}]
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}

"""""""""""""""""""""""""""""""""VimShell""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
let g:vimshell_use_terminal_command = 'urxvtc -e'
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}
"""""""""""""""""""""""""""""""""My Functions""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
augroup HelpInTabs
  autocmd!
  autocmd BufEnter *.txt call HelpInNewTabs()
augroup end

function! HelpInNewTabs()
  if &buftype == 'help'
    execute "normal \<C-w>T"
  endif
endfunction

function! ScratchEdit(cmd, options)
  exe a:cmd tempname()
  setl buftype=nofile bufhidden=wipe nobuflisted
  if !empty(a:options) | exe 'setl' a:options | endif
endfunction

command! -bar -nargs=* Sedit call ScratchEdit('edit', <q-args>)
command! -bar -nargs=* Ssplit call ScratchEdit('split', <q-args>)
command! -bar -nargs=* Svsplit call ScratchEdit('vsplit', <q-args>)
command! -bar -nargs=* Stabedit call ScratchEdit('tabe', <q-args>)

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}
"""""""""""""""""""""""""""""""""""NerdCommenter"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
let g:NERDCustomDelimiters = { 'py' : { 'left': '# ', 'leftAlt': '', 'rightAlt': '' }}
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}
"""""""""""""""""""""""""""""""""""Gundo"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
nnoremap <F5> :GundoToggle<CR>
let g:gundo_right = 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}
"""""""""""""""""""""""""""""""""""Sayonara"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""{{{
nnoremap <leader>q :Sayonara<cr>
nnoremap <leader>Q :Sayonara!<cr>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""}}}
